---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----        Arquivo: cardGanho        ----
----                                  ----
---- ******************************** ----

-- Importando a biblioteca "composer", para gerenciar cenas
local composer = require("composer")
local collection = require("codigos.cardCollection")
local json = require("json")
local cena = composer.newScene()

-- Variáveis
local largura, altura = display.viewableContentWidth, display.viewableContentHeight
local card, versoCard, msg, botao, fundo, brilhoCard, flash, t, g, tabela
local sons, canalBotao, canalNewCard, canalShine

-- Funções locais
---- Carregar arquivo JSON para leitura
local carregarArquivo = function(diretorio)
    local dado = nil
    local caminho = system.pathForFile(diretorio..".json", system.DocumentsDirectory)
    handle = io.open(caminho, "r")

    if handle then
        dado = json.decode(handle:read("*a"), nullval)
        io.close(handle)
    end
    return dado
end

---- Carregar arquivo JSON para escrita
local escreverArquivo = function(diretorio, dados)
    local caminho = system.pathForFile(diretorio..".json", system.DocumentsDirectory)
    local handle = io.open(caminho, "w+")
    if handle then
        handle:write(json.encode(dados))
        io.close(handle)
    end
end

-- Funções de cena
---- Create
function cena:create(evento)
    -- Grupo da cena
	local grupo = self.view

    -- Semeando o random
    math.randomseed(os.time())

    -- Elementos gráficos
    ---- Fundo
	fundo = display.newImageRect("Imagens/telaCardGanho.png", largura, altura)
	fundo.isVisible = false
	grupo:insert(fundo)

    ---- Mensagem "You've Got a New Card!"
    msgSheet = graphics.newImageSheet("Imagens/NEW.jpg", {width = 960, height = 540, numFrames = 2})
    msg = display.newSprite(msgSheet, {name = "new", start = 1, count = 2, time = 400})
    msg.isVisible = false
    grupo:insert(msg)

    ---- Brilho no contorno do card
	brilhoCard = display.newImageRect("Cards/brilho.png", 300, 360)
	brilhoCard.isVisible = false
	grupo:insert(brilhoCard)

	local pontos = evento.params.pontos -- pontos ganhos
	local categoria -- Categoria do card (common, uncommon, rare)
	local count = 0 -- um contador

    -- Determinando qual a categoria de card que o jogador ganhará
    local rdmCategoria = math.random()

	if pontos > 9 and pontos <= 39 then
		categoria = "common"
	elseif pontos > 39 and pontos <= 99 then
        if rdmCategoria > 0.5 then
            categoria = "common"
        else
            categoria = "uncommon"
        end
	elseif pontos > 99 then
        if rdmCategoria <= 0.3 then
            categoria = "common"
        elseif rdmCategoria > 0.3 and rdmCategoria <= 0.7 then
            categoria = "uncommon"
        else
            categoria = "rare"
		end
	end

    rdmCategoria = nil

    -- Checando quantos cards há na categoria escolhida e atribuindo esse valor a "count"
	for i,v in ipairs(collection[categoria]) do
        count = count+1
    end
    -- Cria um número inteiro aleatório entre 1 e a quantidade de cards da categoria
    local rdm = math.random(1,count)

    -- Imagem do verso do card
    versoCard = display.newImage("Cards/CardsFaltando/Verso.png", 0, 0)
    versoCard.isVisible = false
    grupo:insert(versoCard)

    -- O card propriamente dito
    card = display.newImage("Cards/" .. collection[categoria][rdm] .. ".png", 0, 0)
    card.isVisible = false
    grupo:insert(card)

    -- Abre o arquivo "playerCards" e insere-o na variável "tabela", já com o formato de uma tabela Lua
    tabela = carregarArquivo("playerCards")
    if tabela == nil then
        tabela = {}
    end
    -- Insere o novo card ganho dentro da tabela, e para os valores nulos, coloca "false"
    for i = 1, count do
        if tabela[categoria][i] == nil then
            tabela[categoria][i] = false
        end
        tabela[categoria][rdm] = collection[categoria][rdm]
    end
    -- Escreve a tabela acima no arquivo "playerCards"
    escreverArquivo("playerCards", tabela)

    -- Brilho que cobre a tela
    flash = display.newImageRect("Imagens/brilho.png", 960, 540)
    flash.isVisible = false
    grupo:insert(flash)

    -- Botão de confirmação do recebimento do card
    botao = display.newImage("Imagens/Numeros_e_Botoes/ConfirmCard.png", 0, 0)
    botao.isVisible = false
    grupo:insert(botao)

    -- Sons
    sons = {
        bt = audio.loadSound("Músicas/botao.mp3"),
        newcard = audio.loadSound("Músicas/newcard.mp3"),
        shine = nil,
    }
end

---- Show
function cena:show(evento)
	if evento.phase == "will" then
        -- Posicionando os elementos gráficos
        ---- Fundo
		fundo.anchorX, fundo.anchorY = 0, 0
		fundo.x, fundo.y = 0, 0
        fundo.alpha = 0
        fundo.isVisible = true
        transition.to(fundo, {time = 1000, alpha = 1})
        
        ---- Mensagem "You've Got a New Card!"
        msg.anchorX, msg.anchorY = 0.5, 0.5
        msg.x, msg.y = largura/2, altura/2
        
        ---- Brilho no contorno do card
		brilhoCard.anchorX, brilhoCard.anchorY = 0.5, 0.5
        brilhoCard.width, brilhoCard.height = brilhoCard.width * 1.4, brilhoCard.height * 1.4
    	brilhoCard.x, brilhoCard.y = largura/2, altura/2
        
        -- Imagem do verso do card
    	versoCard.anchorX, versoCard.anchorY = 0.5, 0.5
        versoCard.width, versoCard.height = versoCard.width * 1.4, versoCard.height * 1.4
    	versoCard.x, versoCard.y = largura/2, altura/2
        
        -- O card propriamente dito
		card.anchorX, card.anchorY = 0.5, 0.5
        card.width, card.height = card.width * 1.4, card.height * 1.4
    	card.x, card.y = largura/2, altura/2
        
        -- Brilho que cobre a tela
    	flash.anchorX, flash.anchorY = 0.5, 0.5
    	flash.x, flash.y = largura/2, altura/2
    	flash.alpha = 0
        
        -- Botão de confirmação do recebimento do card
    	botao.anchorX, botao.anchorY = 0, 1
    	botao.x, botao.y = card.x + card.width/2 - botao.width/2, card.y - card.height/2 + botao.height/2
	elseif evento.phase == "did" then
		local pai = evento.parent

        -- Mostrando elementos gráficos, mas deixando-os com transparência máxima
        fundo.isVisible = true
        msg.isVisible = true
        card.isVisible = true
        versoCard.isVisible = true
        msg.alpha = 0
        card.alpha = 0
        versoCard.alpha = 0

        -- Função que esconde esta cena e a destrói
		local function escondeOverlay(evento)
			if evento.phase == "began" then
				botao.width = botao.width * 0.9
				botao.height = botao.height * 0.9
			elseif evento.phase == "ended" then
				botao.width = botao.width * 1/0.9
				botao.height = botao.height * 1/0.9

                canalBotao = audio.play(sons["bt"], {channel = 10})

				pai:escondeOverlay()
			end
		end

        -- Funções de animações
        ---- Brilho
		local function brilha()
            flash.isVisible = true
			transition.to(flash, {time = 800, alpha = 1, transition = easing.outSine,
        		onComplete = function()
            		transition.to(flash, {time = 2000, alpha = 0, easing.outExpo,
            			onComplete = function() display.remove(flash); flash = nil; end})
        		end})
		end
        ---- Giro
		local function gira()
			transition.scaleTo(versoCard, {time = 900, xScale = -1, transition = easing.inSine,
        		onComplete = function()
            		display.remove(versoCard)
            		versoCard = nil
            		card.alpha = 1
            		botao.isVisible = true
            		transition.scaleTo(card, {time = 1000, xScale = 1, transition = easing.outSine,
               			onComplete = function()
                    		brilhoCard.isVisible = true
                    		transition.blink(brilhoCard, {time = 2500, transition = easing.outSine})
                		end})
        		end})
		end
        ---- Desaparece
        local function desaparece()
            transition.to(msg, {time = 400, alpha = 0,
                onComplete = function()
                    display.remove(msg)
                    msgSheet = nil
                    msg = nil
                    transition.to(versoCard, {time = 600, alpha = 1,
                        onComplete = function()
                            t = timer.performWithDelay(800, gira, 1)
                            g = timer.performWithDelay(1000, brilha, 1)
                        end})
                end})
            
        end
        ---- Surge
        local function surge()
            msg:play()
            canalNewCard = audio.play(sons["newcard"], {channel = 9})
            transition.to(msg, {time = 500, alpha = 1,
                onComplete = function()
                    t = timer.performWithDelay(2000, desaparece, 1)
                end})
        end

        -- Chama a função "surge"
        surge()
        -- Adiciona o ouvinte de eventos ao botão
		botao:addEventListener("touch", escondeOverlay)
	end
end

---- Destroy
function cena:destroy(evento)
    -- Eliminando tudo da cena da memória
    ---- Valores literais
	largura, altura, t, g, tabela = nil, nil, nil, nil, nil
    ---- Elementos gráficos
    display.remove(botao);  botao = nil;
    display.remove(fundo);  fundo = nil;
    display.remove(brilhoCard); brilhoCard = nil;
    display.remove(card);   card = nil;
    ---- Sons
    canalBotao, canalNewCard, canalShine = nil, nil, nil
    audio.stop()
    audio.dispose(sons["newcard"]);  sons["bg"] = nil;
    audio.dispose(sons["bt"]);  sons["bt"] = nil;
    audio.dispose(sons["shine"]);    sons["sign"] = nil;
    sons = nil
end

-- Ouvintes de eventos da cena
cena:addEventListener("create", cena)
cena:addEventListener("show", cena)
cena:addEventListener("destroy", cena)

return cena