---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----       Arquivo: menuInicial       ----
----                                  ----
---- ******************************** ----

-- Importando a biblioteca "composer", para gerenciar cenas, e criando a cena deste arquivo
local composer = require("composer")
local ads = require("ads")
local cena = composer.newScene()

-- Anúncios
local appIDInterstitial = "ca-app-pub-2494698482505565/8232272932"

local function adListener(evento)
	local msg = event.response

	if evento.phase == "loaded" then
	end

    if (evento.isError) then
    else
    end
end

ads.init("admob", appIDInterstitial, adListener)

-- Variáveis
local fundo, play, cards, about, facebook, sons, flagSom, canalBotao, canalBG, resultadoJogo, resultadoCards, resultadoAbout

-- Funções locais
---- Começa o jogo
local function comecaJogo(evento)
	ads = nil
	-- Remove o ouvinte de eventos do botão play
	play:removeEventListener("tap", comecaJogo)

	-- Toca som de botão pressionado
	canalBotao = audio.play(sons["bt"], {channel = 2})

	-- Troca para a cena do jogo
	composer.gotoScene("codigos.jogo", {effect = "fromRight", time = 400, params = {som = flagSom}})
end
---- Abre o menu de cards
local function abreMenuCards(evento)
	-- Toca som de botão pressionado
	canalBotao = audio.play(sons["bt"], {channel = 2})

	-- Mostra o interstitial
	if math.random() > 0.75 then
		ads.show("interstitial", {appID = appIDInterstitial, testMode = false})
	end

	-- Troca para a cena do menu de cards
	composer.showOverlay("codigos.menuCards", {isModal = true, effect = "fromRight", time = 400})
end

---- Abre o menu de informações
local function abreMenuAbout(evento)
	-- Toca som de botão pressionado
	canalBotao = audio.play(sons["bt"], {channel = 2})

	-- Mostra o interstitial
	if math.random() > 0.75 then
		ads.show("interstitial", {appID = appIDInterstitial, testMode = false})
	end

	-- Troca para a cena do menu de informações
	composer.showOverlay("codigos.menuAbout", {isModal = true, effect = "fromRight", time = 400})
end

---- Abre a página do Facebook no navegador
local function abreURL(evento)
	system.openURL("http://www.facebook.com/panicpig")
end

-- Funções de cena
---- Esconde o menu de cards
function cena:escondeMenuCards()
	composer.hideOverlay("codigos.menuCards")
	ads.hide()
end
---- Esconde o menu de informações
function cena:escondeMenuAbout()
	composer.hideOverlay("codigos.menuAbout")
	ads.hide()
end
---- Create
function cena:create(evento)
	-- Grupo desta cena
	local grupo = self.view

	-- Imagens
	---- Plano de fundo do menu
	fundo = display.newImageRect("Imagens/menu.jpg", 960, 540)
	fundo.isVisible = false
	grupo:insert(fundo)

	---- Botão de play do menu
	play = display.newImage("Imagens/Numeros_e_Botoes/Play.png", 0, 0)
	play.isVisible = false
	grupo:insert(play)

	---- Botão que abre o menu de cards
	cards = display.newImage("Imagens/Numeros_e_Botoes/Cards.png", 0, 0)
	cards.isVisible = false
	grupo:insert(cards)

	---- Botão que abre o menu de Informações
	about = display.newImage("Imagens/Numeros_e_Botoes/about.png", 0, 0)
	about.isVisible = false
	grupo:insert(about)

	---- Botão que redireciona à página do Facebook
	facebook = display.newImageRect("Imagens/Facebook.png", 460, 367)
	facebook.isVisible = false
	grupo:insert(facebook)

	-- Sons
	sons = {
		bg = audio.loadSound("Músicas/open.mp3"),	-- Música de fundo
		bt = audio.loadSound("Músicas/botao.mp3"),	-- Som do botão sendo pressionado
	}
end

---- Show
function cena:show(evento)
	composer.removeScene("codigos.jogo")
	composer.removeScene("codigos.pause")
	composer.removeScene("codigos.fimDeJogo")
	composer.removeScene("codigos.menuCards")

	if evento.params ~= nil then
		flagSom = evento.params.som
	else
		flagSom = "cS"
	end

	if evento.phase == "will" then
		-- Apenas posicionando os elementos gráficos
		---- Fundo
		fundo.anchorX, fundo.anchorY = 0, 0
		fundo.x, fundo.y = 0, 0

		---- Play
		play.anchorX, play.anchorY = 0, 0
		play.x, play.y = 24, 12

		---- Cards
		cards.anchorX, cards.anchorY = 0, 0
		cards.x, cards.y = 24, 24 + cards.height

		---- About
		about.anchorX, about.anchorY = 0, 0
		about.x, about.y = 24, cards.y + cards.height + 12

		---- Facebook
		facebook.anchorX, facebook.anchorY = 0, 0.5
		facebook.x, facebook.y = display.viewableContentWidth/2 - 24, display.viewableContentHeight/2

	elseif evento.phase == "did" then
		-- Altera o volume do áudio
		if flagSom == "cS" then
			audio.setVolume(1)
		elseif flagSom == "sS" then
			audio.setVolume(0)
		end

		-- Mostra fundo e botões
		fundo.isVisible = true
		play.isVisible = true
		cards.isVisible = true
		about.isVisible = true
		facebook.isVisible = true

		-- Toca a música de fundo
		canalBG = audio.play(sons["bg"], {channel = 1, loops = -1})

		-- Carregando o anúncio de tela cheia
		ads.load("interstitial", {appID = appIDInterstitial, testMode = false})

		-- Carregando os menus e o jogo
		resultadoJogo = composer.loadScene("codigos.jogo", true)
		resultadoCards = composer.loadScene("codigos.menuCards", true)
		resultadoAbout = composer.loadScene("codigos.menuAbout", true)

		-- Adiciona ouvinte de evento ao botão play
		play:addEventListener("tap", comecaJogo)
		cards:addEventListener("tap", abreMenuCards)
		about:addEventListener("tap", abreMenuAbout)
		facebook:addEventListener("tap", abreURL)
	end
end

---- Destroy
function cena:destroy(evento)
	-- Eliminando tudo da cena da memória
	---- Valores literais
	resultadoJogo = nil
	resultadoCards = nil
	resultadoAbout = nil
    ---- Elementos gráficos
	display.remove(fundo);		fundo = nil;
	display.remove(play);		play = nil;
	display.remove(cards);		cards = nil;
	display.remove(about);		about = nil;
	---- Cenas
composer.removeScene("codigos.menuCards")
composer.removeScene("codigos.menuAbout")
	---- Sons
	canalBotao, canalBG = nil, nil
    audio.stop()
	audio.dispose(sons["bg"]); 	sons["bg"] = nil;
	audio.dispose(sons["bt"]); 	sons["bt"] = nil;
	sons = nil
end

-- Adicionando os ouvintes de eventos a "cena"
cena:addEventListener("create", cena)
cena:addEventListener("show", cena)
cena:addEventListener("hide", cena)
cena:addEventListener("destroy", cena)

-- Retorna o objeto cena a main
return cena