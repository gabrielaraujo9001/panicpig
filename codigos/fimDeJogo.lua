---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----        Arquivo: fimDeJogo        ----
----                                  ----
---- ******************************** ----

-- Importando a biblioteca "composer", para gerenciar cenas
local composer = require("composer")
local ads = require("ads")
local cena = composer.newScene()

-- Anúncios
local appIDInterstitial = "ca-app-pub-2494698482505565/8232272932"

local function adListener(evento)
	local msg = evento.response

	if evento.phase == "loaded" then
	end

    if (evento.isError) then
    else
    end
end

ads.init("admob", appIDInterstitial, adListener)

-- Variáveis
local largura, altura = display.viewableContentWidth, display.viewableContentHeight
local fundo, topo, textoTopo, numeros, gameover, replay, back, flagSom, sons, canalGO, canalBotao, t

-- Funções locais
---- Criar dígitos
local criarDigitos = function(numDigito)
	local numSheet = graphics.newImageSheet("Imagens/Numeros_e_Botoes/SpriteNumeros.png", {width = 50, height = 50, numFrames = 10})
	local numSprite = display.newSprite(numSheet, {name = "pts", start = 1, count = 10})
	numSprite.x, numSprite.y = (largura/2 + 75 + (numDigito-1) * numSprite.width), 35

	numeros:insert(numDigito, numSprite)
	numeros[numDigito]:setFrame(1)
end

-- Funções de cena
---- Esconder o overlay
function cena:escondeOverlay()
	ads.hide()
	composer.hideOverlay(false, "crossFade", 500)
end

---- Create
function cena:create(evento)
	-- Grupo da cena
	grupo = self.view

	-- Fundo
	fundo = display.newImageRect("Imagens/cenariored.jpg", 960, 540)
	grupo:insert(fundo)

	---- Barra azul no topo da tela
	topo = display.newImage("Imagens/topo.png", 0, 0)
	grupo:insert(topo)

	-- Números
	numeros = display.newGroup()
	grupo:insert(numeros)

	-- Texto do topo
	textoTopo = display.newImage("Imagens/Numeros_e_Botoes/score.png", 0, 0)
	grupo:insert(textoTopo)

	-- Texto de Game Over
	gameover = display.newImage("Imagens/Numeros_e_Botoes/GO.png", 0, 0)
	grupo:insert(gameover)

	-- Botão back (menu inicial)
	back = display.newImage("Imagens/Numeros_e_Botoes/back.png", 0, 0)
	grupo:insert(back)

	-- Botão replay
	replay = display.newImage("Imagens/Numeros_e_Botoes/replay.png", 0, 0)
	grupo:insert(replay)

	-- Sons
	flagSom = evento.params.som;
	sons = {
		GO = audio.loadSound("Músicas/gameover.mp3"),
		bt = audio.loadSound("Músicas/botao.mp3"),
	}

	-- Carregando o anúncio de tela cheia
	ads.load("interstitial", {appID = appIDInterstitial, testMode = false})
end

---- Show
function cena:show(evento)
	if evento.phase == "will" then
		-- Posicionando elementos
		---- Fundo
		fundo.anchorX, fundo.anchorY = 0, 0
		fundo.x, fundo.y = 0, 0

		---- Barra do topo
		topo.anchorX, topo.anchorY = 0.5, 0
		topo.x, topo.y = largura/2, 0

		-- Texto do topo
		textoTopo.anchorX, textoTopo.anchorY = 0, 0
		textoTopo.x, textoTopo.y = 270, 13

		---- Game Over
		gameover.anchorX, gameover.anchorY = 0.5, 1
		gameover.x, gameover.y = largura + gameover.width/2, altura/2

		---- Back
		back.anchorX, back.anchorY = 0.5, 0
		back.x, back.y = -back.width/2, (back.height/2) + (altura/2)

		---- Replay
		replay.anchorX, replay.anchorY = 0.5, 0
		replay.x, replay.y = replay.width/2 + largura, (replay.height/2) + (altura/2)

		-- Animação dos componentes
		transition.to(gameover, {time = 800, x = largura/2, transition = easing.outSine})
		transition.to(back, {time = 800, x = -back.width/1.5 + largura/2, transition = easing.outSine})
		transition.to(replay, {time = 800, x = replay.width/1.5 + largura/2, transition = easing.outSine})
	elseif evento.phase == "did" then
		local pai = evento.parent
		local digitoUm, digitoDois, digitoTres
		local scores = evento.params.pontos
		local auxiliar = evento.params.pontos

		-- Elimina a cena de jogo
		composer.removeScene("codigos.jogo")

		-- Mostra os dígitos na tela
		criarDigitos(1)
		criarDigitos(2)
		criarDigitos(3)

		-- Cálculo dos dígitos individuais
		if auxiliar ~= nil then
			digitoTres = auxiliar % 10
			auxiliar = (auxiliar - digitoTres)/10
			digitoDois = auxiliar % 10
			auxiliar = (auxiliar - digitoDois)/10
			digitoUm = auxiliar % 10
			auxiliar = (auxiliar - digitoUm)/10
			numeros[1]:setFrame(digitoUm + 1)
			numeros[2]:setFrame(digitoDois + 1)
			numeros[3]:setFrame(digitoTres + 1)
			auxiliar = nil
		end

		-- Toca música
		canalGO = audio.play(sons["GO"], {channel = 6})
		
		-- Função para voltar para o menu inicial
		local function voltaMenuInicial(evento)
			ads.hide()
			canalBotao = audio.play(sons["bt"], {channel = 7})
			display.remove(numeros[1])
			display.remove(numeros[2])
			display.remove(numeros[3])
			composer.gotoScene("codigos.menuInicial", {params = {som = flagSom}})
		end

		-- Função para reiniciar o jogo
		local function replayGame(evento)
			ads.hide()
			canalBotao = audio.play(sons["bt"], {channel = 7})
			display.remove(numeros[1])
			display.remove(numeros[2])
			display.remove(numeros[3])
			replay:removeEventListener("tap", replayGame)
			composer.gotoScene("codigos.jogo", {effect = "fromRight", time = 400, params = {som = flagSom}})
		end

		-- Determina se o jogador vai ganhar um card ou não
		local function mostraCardGanho()
			timer.cancel(t)
			t = nil

			-- Mostra tela para ganhar o card
			if (math.random() > 0.34) and (scores > 10) then
				composer.showOverlay("codigos.cardGanho", {isModal = true, effect = "crossFade", time = 300, params = {pontos = scores}})
			end

			-- Mostra o interstitial
			if math.random() > 0.5 then
				ads.show("interstitial", {appID = appIDInterstitial, testMode = false})
			end
			-- Adicionando ouvintes de eventos
			back:addEventListener("tap", voltaMenuInicial)
			replay:addEventListener("tap", replayGame)
		end

		t = timer.performWithDelay(1200, mostraCardGanho, 1)
	end
end

---- Destroy
function cena:destroy(evento)
	-- Eliminando tudo da cena da memória
	---- Valores literais
	largura, altura, pontos, flagSom = nil, nil, nil, nil
    ---- Elementos gráficos
    display.remove(fundo);	fundo = nil;
	display.remove(textoTopo);	textoTopo = nil;
	display.remove(gameover);	gameover = nil;
	display.remove(replay);	replay = nil;
	display.remove(back);	back = nil;
	display.remove(numeros[1])
	display.remove(numeros[2])
	display.remove(numeros[3])
	numeros = nil
	---- Sons
	audio.dispose(sons["GO"]); 	sons["GO"] = nil
	audio.dispose(sons["bt"]); 	sons["bt"] = nil
	sons = nil
end

cena:addEventListener("create", cena)
cena:addEventListener("show", cena)
cena:addEventListener("destroy", cena)

return cena