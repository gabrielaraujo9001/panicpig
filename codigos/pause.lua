---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----          Arquivo: pause          ----
----                                  ----
---- ******************************** ----

-- Importando a biblioteca "composer", para gerenciar cenas
local composer = require("composer")
local ads = require("ads")
local cena = composer.newScene()

-- Anúncios
local appIDBanner = "ca-app-pub-2494698482505565/4430868539"

local function adListener(evento)
	local msg = evento.response

	if evento.phase == "loaded" then
	end

    if (evento.isError) then
    else
    end
end

-- Variáveis
local fundo, resume, back, mute, facebook, splashText, botaoSim, botaoNao, flagSom
local largura, altura = display.viewableContentWidth, display.viewableContentHeight

-- Funções locais (declaração)
local resumeJogo, voltaMenuInicial, mudaAudio

---- Abre a página do Facebook no navegador
local function abreURL(evento)
	system.openURL("http://www.facebook.com/panicpig")
end

-- Funções de cena
---- Create
function cena:create(evento)
	-- Grupo desta cena
	local grupo = self.view

	-- Imagens
	---- Plano de fundo do menu de pause
	fundo = display.newImageRect("Imagens/telapause.png", 960, 540)
	fundo.isVisible = false
	grupo:insert(fundo)

	---- Botão que redireciona à página do Facebook
	facebook  = display.newImageRect("Imagens/Facebook.png", 460, 350)
	facebook.isVisible = false
	grupo:insert(facebook)

	---- Tela que abre quando o jogador clicar no botão back
	splashText = display.newImageRect("Imagens/SplashConfirm/text.png", 567, 425)
	splashText.isVisible = false
	grupo:insert(splashText)

	---- Botão de confirmação
	botaoSim = display.newImageRect("Imagens/SplashConfirm/Yes.png", 140, 140)
	botaoSim.isVisible = false
	grupo:insert(botaoSim)

	---- Botão de negação
	botaoNao = display.newImageRect("Imagens/SplashConfirm/No.png", 140, 140)
	botaoNao.isVisible = false
	grupo:insert(botaoNao)

	---- Botão resume
	resume = display.newImage("Imagens/Numeros_e_Botoes/Return.png", 0, 0)
	resume.isVisible = false
	grupo:insert(resume)

	---- Botão back
	back = display.newImage("Imagens/Numeros_e_Botoes/back.png", 0, 0)
	back.isVisible = false
	grupo:insert(back)

	---- Botão mute
	local muteSheet = graphics.newImageSheet("Imagens/Numeros_e_Botoes/SpriteMute.png", {width = 122, height = 124, numFrames = 2})
	mute = display.newSprite(muteSheet, {name = "m", frames = {1,2}})
	mute.isVisible = false
	grupo:insert(mute)
end

---- Show
function cena:show(evento)
	if evento.phase == "will" then
		-- Apenas posicionando os elementos gráficos
		---- Fundo
		fundo.anchorX, fundo.anchorY = 0, 0
		fundo.x, fundo.y = 0, 0

		---- Facebook
		facebook.anchorX, facebook.anchorY = 0.5, 0.5
		facebook.x, facebook.y = largura/2 + 50, altura/2 + 4

		---- Splash
		splashText.anchorX, splashText.anchorY = 0.5, 0.5
		splashText.x, splashText.y = largura/2 + 50, altura/2

		---- Botão Sim
		botaoSim.anchorX, botaoSim.anchorY = 0.5, 0.5
		botaoSim.x, botaoSim.y = splashText.x - splashText.width/2 + botaoSim.width/2, splashText.y + splashText.height/2 - botaoSim.height/2

		---- Botão Não
		botaoNao.anchorX, botaoNao.anchorY = 0.5, 0.5
		botaoNao.x, botaoNao.y = splashText.x + splashText.width/2 - botaoSim.width/2, splashText.y + splashText.height/2 - botaoNao.height/2

		---- Resume
		resume.anchorX, resume.anchorY = 0.5, 0
		resume.x, resume.y = 81, 14

		---- Mute
		mute.anchorX, mute.anchorY = 0.5, 0
		mute.x, mute.y = 81, 26 + resume.height

		---- Back
		back.anchorX, back.anchorY = 0.5, 0
		back.x, back.y = 81, mute.y + mute.height + 12
	elseif evento.phase == "did" then
		local pai = evento.parent

		-- Mostra elementos
		fundo.isVisible = true
		resume.isVisible = true
		back.isVisible = true
		mute.isVisible = true
		facebook.isVisible = true

		-- Frame do botão mute
		flagSom = pai:getAudioStatus()
		if flagSom == "cS" then
			mute:setFrame(1)
		elseif flagSom == "sS" then
			mute:setFrame(2)
		end

		-- Funções
		--[[Liga ou desliga os sons do jogo. Na verdade, esta função merece uma explicação melhor. Sempre que uma
		cena chama outra cena de sobreposição, esta libera um evento chamado event.parent, ou seja, uma referência
		à cena que chamou a sobreposição. Dessa forma, podemos usar o parent dessa cena para acessar funções da cena
		que o chamou. É isso o que é feito aqui, chamamos uma função dentro da cena "jogo" que liga ou desliga o áudio.
		Claro que essa função não pode ser local, pois não poderíamos chamá-la a partir de outro arquivo]]
		local function mudaAudio(evento)
			if flagSom == "cS" then
				flagSom = "sS"
				mute:setFrame(2)
			elseif flagSom == "sS" then
				flagSom = "cS"
				mute:setFrame(1)
			end
			pai:mudaAudio()
		end

		---- Volta ao jogo (explicação acima é válida para esta função)
		local function resumeJogo(evento)
			-- Esconde os ads
			ads.hide()
			-- Volta pro jogo
			pai:resumeJogo()
		end

		---- Volta para o menu inicial (explicação acima é válida para esta função)
		local function voltaMenuInicial(evento)
			if evento.phase == "began" then
				botaoSim.width = botaoSim.width * 0.9
				botaoSim.height = botaoSim.height * 0.9
			elseif evento.phase == "ended" then
				botaoSim.width = botaoSim.width * 1/0.9
				botaoSim.height = botaoSim.height * 1/0.9
				-- Esconde os ads
				ads.hide()
				-- Remove cena do jogo e vai para cena do menu inicial
				composer.gotoScene("codigos.menuInicial", {params = {som = flagSom}})
				composer.removeScene("codigos.jogo")
			end
		end

		---- Esconde Splash
		local function escondeSplash(evento)
			if evento.phase == "began" then
				botaoNao.width = botaoNao.width * 0.9
				botaoNao.height = botaoNao.height * 0.9
			elseif evento.phase == "ended" then
				botaoNao.width = botaoNao.width * 1/0.9
				botaoNao.height = botaoNao.height * 1/0.9
				splashText.isVisible = false
				botaoSim.isVisible = false
				botaoNao.isVisible = false

				-- Remove ouvintes de eventos aos botões
				botaoSim:removeEventListener("touch", voltaMenuInicial)
				botaoNao:removeEventListener("touch", escondeSplash)
				facebook:addEventListener("tap", abreURL)
			end
		end

		---- Mostra splash screen de confirmação
		local function mostraSplash(evento)
			splashText.isVisible = true
			botaoSim.isVisible = true
			botaoNao.isVisible = true

			-- Adiciona ouvintes de eventos aos botões
			botaoSim:addEventListener("touch", voltaMenuInicial)
			botaoNao:addEventListener("touch", escondeSplash)
			facebook:removeEventListener("tap", abreURL)
		end

		-- Adiciona ouvinte de evento ao botão resume
		mute:addEventListener("tap", mudaAudio)
		resume:addEventListener("tap", resumeJogo)
		back:addEventListener("tap", mostraSplash)
		facebook:addEventListener("tap", abreURL)

		-- Anúncios
		ads.show("banner", {x = 0, y = 5000, appID = appIDBanner, testMode = false})
	end
end

---- Destroy
function cena:destroy(evento)
	-- Eliminando tudo da cena da memória
    ---- Valores literais
	largura, altura, flagSom = nil, nil, nil
	---- Elementos gráficos    	
	display.remove(fundo);		fundo = nil;
	display.remove(resume); 		resume = nil;
	display.remove(back);	back = nil;
	display.remove(mute);	mute = nil;
	display.remove(splashText); 		splashText = nil;
	display.remove(botaoSim);	botaoSim = nil;
	display.remove(botaoNao);	botaoNao = nil;
end

-- Adicionando os ouvintes de eventos a "cena"
cena:addEventListener("create", cena)
cena:addEventListener("show", cena)
cena:addEventListener("destroy", cena)

-- Retorna o objeto cena a main
return cena