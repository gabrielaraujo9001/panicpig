---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----          Arquivo: jogo           ----
----                                  ----
---- ******************************** ----

-- Importando bibliotecas
local composer = require("composer")	-- Responsável por gerenciar cenas
local cena = composer.newScene()	-- A cena propriamente dita
local math = require("math")	-- Contém funções matemáticas
local physics = require("physics")	-- Trata da física do jogo
local json = require("json")

-- Variáveis
---- Valores notáveis
local largura = display.viewableContentWidth
local altura = display.viewableContentHeight
local centroX = largura/2
local centroY = altura/2

---- Grupos
local botoes, inimigos, numeros, sons

---- Elementos fixos
local fundo, pause, linhaFinal, topo, textoTopo, barra, sensor, pig, vidas, vidasImg

---- Sheets para sprites
local pirataSheet, alienSheet, karatecaSheet, orcSheet

---- Sons e músicas
local canalBotao, canalSign, canalBG

---- Outros valores
local flagSom -- Poderá ter dois valores de estado: "sS", que significa "sem som", ou "cS", "com som"
local dadosDeSequencia, scores, temporizador, resultadoFim, resultadoPause, wss

-- Funções locais (declaração)
local criarDigitos
local pausaJogo
local criarSprites
local mudaRoupaPig
local criarVilao
local update
local atribuiPontos
local aoColidir
local destruirObjeto
local criaPigChorando
local fimDeJogo

-- Funções de cena customizadas
---- Devolve flagSom
function cena:getAudioStatus()
	return flagSom
end
---- Liga e desliga áudio
function cena:mudaAudio()
	if flagSom == "sS" then
		audio.setVolume(1)
		flagSom = "cS"
	elseif flagSom == "cS" then
		audio.setVolume(0)
		flagSom = "sS"
	end
end

---- Continua o jogo
function cena:resumeJogo()
	composer.hideOverlay()
	canalBotao = audio.play(sons["bt"], {channel = 4})

	if(inimigos ~= nil) then
		for i = 1, inimigos.numChildren do
			inimigos[i]:play()
		end
	end

	sensor:addEventListener("collision", aoColidir)
	linhaFinal:addEventListener("collision", destruirObjeto)
	Runtime:addEventListener("enterFrame", update)
	timer.resume(temporizador)
end

-- Funções de cena
---- Create
function cena:create(evento)
	-- Grupo desta cena
	local grupo = self.view

	-- Semeia uma sequência aleatória
	math.randomseed(os.time())

	-- Imagens
	---- Plano de fundo do menu
	fundo = display.newImageRect("Imagens/cenariored.jpg", 960, 540)
	fundo.isVisible = true
	grupo:insert(fundo)

	---- Barra azul no topo da tela
	topo = display.newImage("Imagens/topo.png", 0, 0)
	topo.isVisible = true
	grupo:insert(topo)

	---- Texto "SCORES"
	textoTopo = display.newImage("Imagens/Numeros_e_Botoes/score.png", 0, 0)
	textoTopo.isVisible = false
	grupo:insert(textoTopo)

	---- Botão de pause do jogo
	pause = display.newImage("Imagens/Numeros_e_Botoes/Pause.png", 0, 0)
	pause.isVisible = false
	grupo:insert(pause)

	---- Vidas
	local caminho = system.pathForFile("life.json", system.DocumentsDirectory)
	local handle = io.open(caminho, "r")
	local dado
	if handle then
		dado = json.decode(handle:read("*a"))
	end
	io.close(handle)
	caminho = nil
	handle = nil

	vidas = dado["quantidadeDeVidas"]
	dado = nil

	if vidas == 1 then
		vidasImg = display.newImage("Imagens/Vida.png", 0, 0)
		vidasImg.isVisible = false
		grupo:insert(vidasImg)
	end

	---- Pig
	local pigSheet = graphics.newImageSheet("Imagens/Porcos/SpritePig.png", {width = 203, height = 287, numFrames = 6})
	pig = display.newSprite(pigSheet, {name = "pig", start = 1, count = 6})
	pig.isVisible = true
	grupo:insert(pig)

	---- Sensor que detectará a passagem do vilão pelo porco
	sensor = display.newRect(0, 0, 12, 50)
	sensor:setFillColor(0,0,0,0)
	grupo:insert(sensor)

	---- Grupo que armazenará os vilões
	inimigos = display.newGroup()
	grupo:insert(inimigos)

	---- Linha que destrói os vilões
	linhaFinal = display.newLine(largura, 0, largura, altura)
	grupo:insert(linhaFinal)

	---- Grupo que armazenará os botões
	botoes = display.newGroup()
	grupo:insert(botoes)

	---- Barra azul inferior
	barra = display.newImage("Imagens/barra.png", 0, 0)
	barra.isVisible = true
	botoes:insert(barra)

	---- Botões de troca de roupa
	botaoPirata = display.newImageRect("Imagens/Icones/Pirate.png", 163, 137)
	botaoAlien = display.newImageRect("Imagens/Icones/Astro.png", 163, 137)
	botaoKarateca = display.newImageRect("Imagens/Icones/Ninja.png", 163, 137)
	botaoOrc = display.newImageRect("Imagens/Icones/Barbarian.png", 163, 137)
	botaoPirata.isVisible = false
	botaoAlien.isVisible = false
	botaoKarateca.isVisible = false
	botaoOrc.isVisible = false
	botoes:insert(botaoPirata)
	botoes:insert(botaoAlien)
	botoes:insert(botaoKarateca)
	botoes:insert(botaoOrc)

	-- Sons
	flagSom = evento.params.som
	sons = {
		bg = audio.loadSound("Músicas/bg.mp3"),	-- Música de fundo
		bt = audio.loadSound("Músicas/botao.mp3"),	-- Som do botão sendo pressionado
		sign = audio.loadSound("Músicas/s3.mp3"),	-- Som da troca de roupa com sucesso
	}
end

---- Show
function cena:show(evento)
	local grupo = self.view

	if evento.params ~= nil then
		flagSom = evento.params.som
	end

	if evento.phase == "will" then
		-- Apenas posicionando os elementos gráficos
		---- Fundo
		fundo.anchorX, fundo.anchorY = 0, 0
		fundo.x, fundo.y = 0, 0

		---- Barra do topo
		topo.anchorX, topo.anchorY = 0.5, 0
		topo.x, topo.y = centroX, 0

		---- Texto do topo
		textoTopo.anchorX, textoTopo.anchorY = 0, 0
		textoTopo.x, textoTopo.y = 270, 13

		---- Pause
		pause.anchorX, pause.anchorY = 0.5, 0
		pause.x, pause.y = largura - pause.width, 0

		---- Vidas
		if vidasImg ~= nil then
			vidasImg.anchorX, vidasImg.anchorY = 0, 0
			vidasImg.x, vidasImg.y = 12, 12
		end

		---- Pig
		pig.anchorX, pig.anchorY = 0.5, 0.5
		pig.x, pig.y = centroX + 152, centroY + 12
		
		---- Sensor
		sensor.x, sensor.y = pig.x - pig.width/2, pig.y

		---- Barra
		barra.anchorX, barra.anchorY = 0.5, 1
		barra.x, barra.y = centroX, altura

		---- Botões de troca de roupa
		botaoPirata.x, botaoPirata.y = centroX - botaoPirata.width * 3/2, altura - botaoPirata.height/2
		botaoAlien.x, botaoAlien.y = centroX - botaoAlien.width/2, altura - botaoAlien.height/2
		botaoKarateca.x, botaoKarateca.y = centroX + botaoKarateca.width/2, altura - botaoKarateca.height/2
		botaoOrc.x, botaoOrc.y = centroX + botaoOrc.width * 3/2, altura - botaoOrc.height/2
	elseif evento.phase == "did" then
		-- Elimina as cenas que existirem
		composer.removeScene("codigos.menuInicial")
		composer.removeScene("codigos.fimDeJogo")

		resultadoPause = composer.loadScene("codigos.pause", true)

		-- Alterar o volume do áudio
		if flagSom == "cS" then
			audio.setVolume(1)
		elseif flagSom == "sS" then
			audio.setVolume(0)
		end

		-- Resumindo a física do jogo, que estava pausada
		physics.start();
		physics.setGravity(0, 0)  -- Põe como zero o valor da gravidade, pois ela não será necessária

		-- Mostrando os elementos gráficos
		textoTopo.isVisible = true
		pause.isVisible = true
		botaoPirata.isVisible = true
		botaoAlien.isVisible = true
		botaoKarateca.isVisible = true
		botaoOrc.isVisible = true
		if vidasImg ~= nil then
			vidasImg.isVisible = true
		end

		-- Inicializando alguns elementos
		---- Pig
		pig.frameAtual = 1
		pig:setFrame(pig.frameAtual)

		---- Sensor
		sensor.flag = "naked"
		physics.addBody(sensor)
		sensor.isSensor = true
		sensor:addEventListener("collision", aoColidir)

		---- Linha final
		physics.addBody(linhaFinal)
		linhaFinal.isSensor = true
		linhaFinal:addEventListener("collision", destruirObjeto)

		---- Botões de troca de roupa
		botaoPirata.nome = "botaoPirata"
		botaoAlien.nome = "botaoAlien"
		botaoKarateca.nome = "botaoKarateca"
		botaoOrc.nome = "botaoOrc"

		-- Sprite de inimigos e grupo de inimigos
		criarSprites()

		-- Dígitos
		numeros = {}
		criarDigitos(1)
		criarDigitos(2)
		criarDigitos(3)
		grupo:insert(numeros[1])
		grupo:insert(numeros[2])
		grupo:insert(numeros[3])

		-- Os pontos propriamente ditos
		scores = 0

		-- Aciona a música de fundo
		canalBG = audio.play(sons["bg"], {channel = 3, loops = -1})

		-- Aciona temporizador que criará os vilões
		temporizador = timer.performWithDelay(2000, criarVilao, 0)

		-- Adiciona ouvintes de eventos
		---- Botões
		pause:addEventListener("tap", pausaJogo)
		botaoPirata:addEventListener("tap", mudaRoupaPig)
		botaoAlien:addEventListener("tap", mudaRoupaPig)
		botaoKarateca:addEventListener("tap", mudaRoupaPig)
		botaoOrc:addEventListener("tap", mudaRoupaPig)
		---- Runtime
		Runtime:addEventListener("enterFrame", update)
	end
end

---- Destroy
function cena:destroy(evento)
	-- Eliminando tudo da cena da memória
    ---- Valores literais
	largura, altura = nil, nil
	botoes, inimigos, numeros = nil, nil, nil
	pirataSheet, alienSheet, karatecaSheet, orcSheet = nil, nil, nil, nil
	dadosDeSequencia, scores, temporizador, resultadoFim = nil, nil, nil, nil
	---- Elementos gráficos
	display.remove(fundo);		fundo = nil;
	display.remove(pause);		pause = nil;
	display.remove(linhaFinal);		linhaFinal = nil;
	display.remove(topo);		topo = nil;
	display.remove(textoTopo);		textoTopo = nil;
	display.remove(barra);		barra = nil;
	display.remove(vidasImg);		vidasImg = nil;
	---- Sons
	audio.stop()
	canalBotao, canalSign, canalBG = nil, nil, nil
	audio.dispose(sons["bg"]); 	sons["bg"] = nil;
	audio.dispose(sons["bt"]); 	sons["bt"] = nil;
	audio.dispose(sons["sign"]); 	sons["sign"] = nil;
	sons = nil
end

-- Funções locais (escopo)
---- Aciona a cena de pause
pausaJogo = function()
	-- Pausa as sprites
	if(inimigos ~= nil) then
		for i = 1, inimigos.numChildren do
			inimigos[i]:pause()
		end
	end
	
	-- Remove os ouvintes de eventos
	sensor:removeEventListener("collision", aoColidir)
	linhaFinal:removeEventListener("collision", destruirObjeto)
	Runtime:removeEventListener("enterFrame", update)
	timer.pause(temporizador)

	-- Chama a cena de pause
	composer.showOverlay("codigos.pause", {isModal = true})
end

---- Cria vários atributos para sprites
criarSprites = function()
	dadosDeSequencia = {
		{name = "1", frames = {1, 2, 3, 2}, time = 750},
		{name = "2", frames = {4, 5, 6, 5}, time = 750},
		{name = "3", frames = {7, 8, 9, 8}, time = 750},
		{name = "4", frames = {10, 11, 12, 11}, time = 750},
	}

	pirataSheet = graphics.newImageSheet("Imagens/Viloes/SpritePirata.png", {width = 157, height = 328, numFrames = 12})
	alienSheet = graphics.newImageSheet("Imagens/Viloes/SpriteAlien.png", {width = 211, height = 375, numFrames = 6})
	karatecaSheet = graphics.newImageSheet("Imagens/Viloes/SpriteKarateca.png", {width = 157, height = 328, numFrames = 12})
	orcSheet = graphics.newImageSheet("Imagens/Viloes/SpriteOrc.png", {width = 383, height = 422, numFrames = 9})
end

---- Cria e põe na tela os dígitos que mostrarão a pontuação
criarDigitos = function(numDigito)
	local numSheet = graphics.newImageSheet("Imagens/Numeros_e_Botoes/SpriteNumeros.png", {width = 50, height = 50, numFrames = 10})
	local numSprite = display.newSprite(numSheet, {name = "pts", start = 1, count = 10})
	numSprite.x, numSprite.y = (centroX + 75 + (numDigito-1) * numSprite.width), 35

	table.insert(numeros, numDigito, numSprite)
	numeros[numDigito]:setFrame(1)
end

---- Função que altera a roupa do Pig
mudaRoupaPig = function(evento)
	if evento.target.nome == "botaoPirata" then
		pig.frameAtual = 2
		sensor.flag = "pirata"
	end

	if evento.target.nome == "botaoAlien" then
		pig.frameAtual = 3
		sensor.flag = "alien"
	end

	if evento.target.nome == "botaoKarateca" then
		pig.frameAtual = 4
		sensor.flag = "karateca"
	end

	if evento.target.nome == "botaoOrc" then
		pig.frameAtual = 5
		sensor.flag = "orc"
	end

	canalBotao = audio.play(sons["bt"], {channel = 4})
	pig:setFrame(pig.frameAtual)
end

---- Função que cria os inimigos e os adiciona a um grupo chamado 'inimigos']]
criarVilao = function()
	local vilao
	local aleatorio, posicaoY = math.random(), 460

	if(aleatorio < 0.35) then
		-- Pirata
		local rnd = math.random(1,4)
		vilao = display.newSprite(pirataSheet, dadosDeSequencia)
		vilao.flag = "pirata"
		vilao:setSequence(tostring(rnd))
	elseif(aleatorio >= 0.35 and aleatorio < 0.55) then
		-- Alien
		local rnd = math.random(1,2)
		vilao = display.newSprite(alienSheet, dadosDeSequencia)
		vilao.flag = "alien"
		vilao:setSequence(tostring(rnd))
	elseif(aleatorio >= 0.55 and aleatorio < 0.75) then
		-- Karateca
		local rnd = math.random(1,4)
		vilao = display.newSprite(karatecaSheet, dadosDeSequencia)
		vilao.flag = "karateca"
		vilao:setSequence(tostring(rnd))
	else
		-- Orc
		local rnd = math.random(1,3)
		vilao = display.newSprite(orcSheet, dadosDeSequencia)
		vilao.flag = "orc"
		vilao:setSequence(tostring(rnd))
	end
	-- Posiciona o vilão
	vilao.anchorX, vilao.anchorY = 0.5, 1
	vilao.x, vilao.y = -vilao.width/2, posicaoY
	vilao.velocidade = math.random()*10 + 6 -- velocidade mínima = 6px/frame (180px/s), velocidade máxima = 16px/frame (480px/s)
	-- Adiciona física ao vilão
	physics.addBody(vilao)
	vilao.isSensor = true
	-- Aciona a sprite do vilão
	vilao:play()
	-- Insere o vilão no grupo "inimigos", que está inserido em "cena"
	inimigos:insert(vilao)
end

---- Move todos os vilões dentro do grupo e o pig chorando
update = function(evento)
	if(inimigos ~= nil) then
		for i = 1, inimigos.numChildren do
			if(inimigos[i].x < largura + inimigos[i].width/2) then
				inimigos[i].x = inimigos[i].x + inimigos[i].velocidade
			end
		end
	end

	if(pigChorando ~= nil) then
		if(pigChorando.x < largura + pigChorando.width/2) then
			pigChorando.x = pigChorando.x + pigChorando.velocidade
		else
			display.remove(pigChorando)
			pigChorando = nil
			fimDeJogo()
		end
	end
end

---- Função que atualizará a pontuação
atribuiPontos = function(auxiliar)
	local digitoUm, digitoDois, digitoTres
	digitoTres = auxiliar % 10 -- "digitoTres" = 352 % 10 = 2
	auxiliar = (auxiliar - digitoTres)/10 -- "auxiliar" = (352 - 2)/10 = 350 / 10 = 35
	digitoDois = auxiliar % 10 -- "digitoDois" = 35 % 10 = 5
	auxiliar = (auxiliar - digitoDois)/10 -- "auxiliar" = (35 - 5)/10 = 30 / 10 = 3
	digitoUm = auxiliar % 10 -- "digitoUm" = 3 % 10 = 3
	auxiliar = (auxiliar - digitoUm)/10 -- "auxiliar" = (3 - 3)/10 = 0 / 10 = 0
	numeros[1]:setFrame(digitoUm + 1) --[[Aqui indicamos à table "numeros" que queremos que o frame da
	sprite 1 seja setado para "digitoUm" + 1, ou seja, 3+1 = 4. Por que 4? Porque os números guardados
	nas sprites vão de 0 a 9, mas o primeiro frame de uma sprite é sempre 1. Portanto, imagem 0 = frame 1,
	imagem 1 = frame 2, imagem 9 = frame 10. Essa explicação é válida para as duas linhas de código abaixo.
	Detalhe: nada disso interfere com o valor da pontuação, pois este está guardado em "scores"]]
	numeros[2]:setFrame(digitoDois + 1)
	numeros[3]:setFrame(digitoTres + 1)
	auxiliar = nil
	transition.scaleTo(numeros[1], {xScale = 1.3, yScale = 1.3, time = 150, onComplete = function()
		transition.scaleTo(numeros[1], {xScale = 1, yScale = 1, time = 150}) end})
	transition.scaleTo(numeros[2], {xScale = 1.3, yScale = 1.3, time = 150, onComplete = function()
		transition.scaleTo(numeros[2], {xScale = 1, yScale = 1, time = 150}) end})
	transition.scaleTo(numeros[3], {xScale = 1.3, yScale = 1.3, time = 150, onComplete = function()
		transition.scaleTo(numeros[3], {xScale = 1, yScale = 1, time = 150}) end})
end

---- Ações que serão tomadas na detecção da colisão
aoColidir = function(evento)
	if evento.phase == "ended" then
		-- Atribui o valor da velocidade do vilão que "tocou" o pig
		local vel = evento.other.velocidade
		if evento.target.flag == evento.other.flag or evento.target.flag == "coringa" then
			canalSign = audio.play(sons["sign"], {channel = 5})
			--[[Aqui será utilizado um pouco de cálculo para determinar que número (dígito) aparecerá
			na tela. Apenas como exemplo, utilizarei o número 352 como score]]
			scores = scores + 1 -- Digamos que após essa linha o valor de "scores" se tornou 352
			atribuiPontos(scores)

			if wss ~= nil then
				display.remove(wss)
				wss = nil
			end

			-- Troca o Pluk de volta para Naked (se o Coringa tiver sido usado)
			if evento.target.flag == "coringa" then
				pig.frameAtual = 1
				pig:setFrame(pig.frameAtual)
				sensor.flag = "naked"
			end
		elseif evento.target.flag ~= evento.other.flag and vidasImg ~= nil then
			-- Removendo os ouvintes e o temporizador
			Runtime:removeEventListener("enterFrame", update)
			timer.pause(temporizador)

			-- Pausa as sprites dos vilões
			if(inimigos ~= nil) then
				for i = 1, inimigos.numChildren do
					inimigos[i]:pause()
				end
			end

			-- Mostrando a tela de confirmação, assim como os botões SIM e NÃO
			local splashVida = display.newImage("Imagens/splashVida.png", 0, 0)
			splashVida.x, splashVida.y = largura/2, altura/2
			local sim = display.newImage("Imagens/SplashConfirm/Yes.png", 0, 0)
			sim.x, sim.y = largura/2 - 200, altura/2
			local nao = display.newImage("Imagens/SplashConfirm/No.png", 0, 0)
			nao.x, nao.y = largura/2 + 200, altura/2

			-- Função que, quando o botão SIM for clicado, vestirá o Pig com a roupa do Coringa
			local function mostraCoringa(evento)
				-- Remove os elementos gráficos da splash e o ícone da vida
				display.remove(vidasImg);		vidasImg = nil;
				display.remove(splashVida);		splashVida = nil;
				display.remove(sim);		sim = nil;
				display.remove(nao);		nao = nil;

				-- Soma a pontuação
				scores = scores + 1

				-- Muda a roupa do Pig para a do Coringa e modifica a flag do sensor
				pig.frameAtual = 6
				pig:setFrame(pig.frameAtual)
				sensor.flag = "coringa"

				-- Abre o arquivo life.json com o intuito de excluir a vida de dentro dele
				local caminho = system.pathForFile("life.json", system.DocumentsDirectory)
				local handle = io.open(caminho, "w")
				local dado = {quantidadeDeVidas = 0}
				if handle then
					handle:write(json.encode(dado))
					io.close(handle)
				end
				caminho = nil
				handle = nil
				vidas = dado["quantidadeDeVidas"]
				dado = nil

				-- Mostra a mensagem "Why So Serious?" na tela
				wss = display.newImage("Imagens/Why.png", 0, 0)
				wss.x, wss.y = pig.x - wss.width/1.5, altura/2 - wss.height
				wss.alpha = 0

				-- Produz uma animação com a mensagem. Quando a animação acabar, o jogo volta ao normal
				transition.to(wss, {time = 2000, alpha = 1, onComplete = function()
					atribuiPontos(scores)

					-- Continua as sprites
					if(inimigos ~= nil) then
						for i = 1, inimigos.numChildren do
							inimigos[i]:play()
						end
					end

					Runtime:addEventListener("enterFrame", update)
					timer.resume(temporizador)
				end})
			end

			local function cancelaCoringa(evento)
				display.remove(splashVida)
				splashVida = nil
				display.remove(sim)
				sim = nil
				display.remove(nao)
				nao = nil

				-- Continua as sprites
				if(inimigos ~= nil) then
					for i = 1, inimigos.numChildren do
						inimigos[i]:play()
					end
				end
				
				Runtime:addEventListener("enterFrame", update)
				timer.resume(temporizador)

				criaPigChorando(vel)
			end

			nao:addEventListener("tap", cancelaCoringa)
			sim:addEventListener("tap", mostraCoringa)
		else
			-- Chama a função criaPigChorando, passando como parâmetro "vel"
			criaPigChorando(vel)
		end
	end
end

---- Função para destruir os objetos que atravessarem a tela
destruirObjeto = function(evento)
	if(evento.phase == "ended") then
		evento.other:removeSelf()
		evento.other = nil
	end
end

---- Cria o pig chorando e remove alguns ouvintes de eventos
criaPigChorando = function(vel)
	-- Cria pig chorando
	pigChorando = display.newImageRect("Imagens/Porcos/Cry.png", 285, 278, 0, 0)
	pigChorando.x, pigChorando.y = pig.x - pigChorando.width/1.5, pig.y - pigChorando.height/2
	pigChorando.velocidade = vel

	-- Esconde pig
	pig.isVisible = false

	-- Para várias funções
	---- Ouvintes de eventos dos botões
	pause:removeEventListener("tap", pausaJogo)
	botaoPirata:removeEventListener("tap", mudaRoupaPig)
	botaoAlien:removeEventListener("tap", mudaRoupaPig)
	botaoKarateca:removeEventListener("tap", mudaRoupaPig)
	botaoOrc:removeEventListener("tap", mudaRoupaPig)
	---- Ouvinte de evento do sensor
	sensor:removeEventListener("collision", aoColidir)
	---- Para o temporizador
	timer.cancel(temporizador)

	-- Pré-carrega a cena de fim de jogo
	resultadoFim = composer.loadScene("codigos.fimDeJogo", true)
end

---- Termina o resto dos eventos e chama pela cena de fim de jogo
fimDeJogo = function()
	-- Removendo elementos gráficos
	display.remove(inimigos)
	inimigos = nil

	-- Para o evento de Runtime e o áudio
	audio.stop(canalBG)
	Runtime:removeEventListener("enterFrame", update)

	-- Animando alguns elementos
	transition.to(botoes, {time = 400, y = altura + botoes.height/2, transition = easing.outSine, onComplete = function()
		display.remove(botoes); botoes = nil; end})
	transition.to(pause, {time = 400, x = largura + pause.width/2, transition = easing.outSine, onComplete = function()
		display.remove(pause)
		pause = nil
		composer.gotoScene("codigos.fimDeJogo", {params = {pontos = scores, som = flagSom}})
		end
		}
	)
end

-- Adicionando os ouvintes de eventos a "cena"
cena:addEventListener("create", cena)
cena:addEventListener("show", cena)
cena:addEventListener("destroy", cena)

-- Retorna o objeto cena a main
return cena