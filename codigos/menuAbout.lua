---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----        Arquivo: menuAbout        ----
----                                  ----
---- ******************************** ----

-- Importando a biblioteca "composer", para gerenciar cenas, e criando a cena deste arquivo
local composer = require("composer")
local ads = require("ads")
local widget = require("widget")
local cena = composer.newScene()

-- Anúncios
local appIDBanner = "ca-app-pub-2494698482505565/4430868539"

local function adListener(evento)
	local msg = evento.response

	if evento.phase == "loaded" then
	end

    if (evento.isError) then
    else
    end
end

-- Variáveis
local largura, altura = display.viewableContentWidth, display.viewableContentHeight
local fundo, back, info
local scrollView

-- Funções locais
---- Ouvinte do scrollView
local function scrollListener(evento)
	local phase = evento.phase
	local direction = evento.direction
	
	-- Se scrollView atingiu seu limite
	if evento.limitReached then
		if "up" == direction then
		elseif "down" == direction then
		elseif "left" == direction then
		elseif "right" == direction then
		end
	end
			
	return true
end

-- Funções de cena
---- Create
function cena:create(evento)
	-- Grupo da cena
	grupo = self.view

	-- Elementos gráficos
	---- Fundo
	fundo = display.newImageRect("Imagens/TelaCards/background.png", largura, altura)
	fundo.isVisible = false
	grupo:insert(fundo)

	---- Botão back
	back = display.newImage("Imagens/TelaCards/back.png", 0, 0)
	back.isVisible = false
	grupo:insert(back)

	-- Scroll
	scrollView = widget.newScrollView{
		left = 0,
		top = 0,
		width = display.contentWidth,
		height = display.contentHeight,
		bottomPadding = 100,
		friction = 0.94,
		id = "onBottom",
		horizontalScrollDisabled = true,
		verticalScrollDisabled = false,
		listener = scrollListener,
		hideBackground = true,
	}

	-- Informações
	info = display.newImage("Imagens/TelaAbout/About.png", 0, 0)
	info.anchorX, info.anchorY = 0.5, 0
	info.x, info.y = largura/2, 48
	scrollView:insert(info)

	scrollView.isVisible = false
	grupo:insert(scrollView)
end

---- Show
function cena:show(evento)
	local pai = evento.parent

	if evento.phase == "will" then
		-- Posicionando objetos
		---- Fundo
		fundo.anchorX, fundo.anchorY = 0, 0
		fundo.x, fundo.y = 0, 0

		---- Back
		back.anchorX, back.anchorY = 0.5, 0.5
		back.x, back.y = back.width/2 + 12, back.height/2 + 6		

	elseif evento.phase == "did" then
		-- Anúncios
		ads.show("banner", {x = 0, y = 5000, appID = appIDBanner, testMode = false})

		-- Tornando os objetos visíveis
		fundo.isVisible = true
		back.isVisible = true
		scrollView.isVisible = true

		local function voltaMenuInicial(evento)
			-- Esconde os ads
			ads.hide()
			pai:escondeMenuAbout()
		end

		back:addEventListener("tap", voltaMenuInicial)
	end
end

---- Destroy
function cena:destroy(evento)
end

-- Adicionando ouvintes de eventos a cena
cena:addEventListener("create", cena)
cena:addEventListener("show", cena)
cena:addEventListener("destroy", cena)

-- Retornando o objeto cena
return cena