---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----          Arquivo: splash         ----
----                                  ----
---- ******************************** ----

-- Importando a biblioteca "composer", para gerenciar cenas
local composer = require("composer")
local json = require("json")
local cena = composer.newScene()

-- Variáveis
local fundo, texto, caminho, handle, resultado, pisca

-- Funções locais
local function mostraMenu()
	-- Excluindo o texto
	transition.cancel(pisca)
	display.remove(texto)
	texto = nil
	-- Testando se existe o arquivo playerCards.json. Se não existir, ele será criado
	caminho = system.pathForFile("playerCards.json", system.DocumentsDirectory)
	handle = io.open(caminho, "r")
	if handle == nil then
		local tabela = {common = {}, uncommon = {}, rare = {}}
		handle = io.open(caminho, "w")
		handle:write(json.encode(tabela))
	end
	io.close(handle)

	-- Testando se existe o arquivo life.json. Se não existir, ele será criado
	caminho = system.pathForFile("life.json", system.DocumentsDirectory)
	handle = io.open(caminho, "r")
	if handle == nil then
		local tabela = {quantidadeDeVidas = 0}
		handle = io.open(caminho, "w")
		handle:write(json.encode(tabela))
	end
	io.close(handle)

	caminho = nil
	handle = nil

	composer.gotoScene("codigos.menuInicial")
end

-- Funções de cena
---- Create
function cena:create(evento)
	print("Cena sendo carregada")
	-- Grupo desta cena
	local grupo = self.view

	-- Imagens
	---- Plano de fundo do menu de pause
	fundo = display.newImageRect("Imagens/Splashpig.jpg", 960, 540)
	fundo.isVisible = false
	grupo:insert(fundo)
end

---- Show
function cena:show(evento)
	if evento.phase == "will" then
		-- Apenas posicionando os elementos gráficos
		---- Fundo
		fundo.anchorX, fundo.anchorY = 0, 0
		fundo.x, fundo.y = 0, 0
	elseif evento.phase == "did" then
		-- Mostra elemento
		fundo.isVisible = true

		---- Texto
		texto = display.newText("TOUCH TO START", display.viewableContentWidth/2, display.viewableContentHeight/2 + 72, native.systemFont, 36)
		texto:setFillColor(0,0,0)

		pisca = transition.blink(texto, {time = 2000})

		resultado = composer.loadScene("codigos.menuInicial")

		-- Adiciona ouvinte de evento
		fundo:addEventListener("tap", mostraMenu)
	end
end

---- Destroy
function cena:destroy(evento)
	-- Eliminando tudo da cena da memória
    ---- Elementos gráficos
    display.remove(fundo)
    fundo = nil
end

-- Adicionando os ouvintes de eventos a "cena"
cena:addEventListener("create", cena)
cena:addEventListener("show", cena)
cena:addEventListener("destroy", cena)

-- Retorna o objeto cena a main
return cena