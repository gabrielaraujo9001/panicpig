---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----        Arquivo: menuCards        ----
----                                  ----
---- ******************************** ----

-- Importando bibliotecas
local composer = require("composer")
local ads = require("ads")
local widget = require("widget")
local json = require("json")

-- Criando a cena
local cena = composer.newScene()

-- Anúncios
local appIDBanner = "ca-app-pub-2494698482505565/4430868539"

local function adListener(evento)
	local msg = evento.response

	if evento.phase == "loaded" then
	end

    if (evento.isError) then
    else
    end
end

-- Variáveis
local largura, altura = display.viewableContentWidth, display.viewableContentHeight
local fundo, back, label, versoCards, frenteCards, tabela, sons, canalBotao
local scrollView

-- Funções locais
---- Ouvinte do scrollView
local function scrollListener(evento)
	local phase = evento.phase
	local direction = evento.direction
	
	-- Se scrollView atingiu seu limite
	if evento.limitReached then
		if "up" == direction then
		elseif "down" == direction then
		elseif "left" == direction then
		elseif "right" == direction then
		end
	end
			
	return true
end

---- Carregar arquivo JSON para leitura
local function carregarArquivo(nome)
    local dado = nil
    local caminho = system.pathForFile(nome..".json", system.DocumentsDirectory)
    handle = io.open(caminho, "r")

    if handle then
        dado = json.decode(handle:read("*a"))
        io.close(handle)
    end
    return dado
end

-- Funções de cena
---- Create
function cena:create(evento)
	grupo = self.view
	local count = 0
	local categoria = "common"
	local posX, posY = 0, 0

	-- Fundo
	fundo = display.newImageRect("Imagens/TelaCards/background.png", 960, 540)
	fundo.isVisible = false
	grupo:insert(fundo)

	-- Label
	label = display.newImage("Imagens/TelaCards/labelT.png", 0, 0)
	label.isVisible = false

	-- Scroll
	scrollView = widget.newScrollView{
		left = 0,
		top = 0,
		width = display.contentWidth,
		height = display.contentHeight,
		bottomPadding = 170,
		friction = 0.94,
		id = "onBottom",
		horizontalScrollDisabled = true,
		verticalScrollDisabled = false,
		listener = scrollListener,
		hideBackground = true,
	}

	versoCards = display.newGroup()
	for i = 1, 39 do
		local novoVersoCard = display.newImage("Cards/CardsFaltando/" .. i .. ".png", 0, 0)
		novoVersoCard.anchorX, novoVersoCard.anchorY = 0, 0
		novoVersoCard.x, novoVersoCard.y = 73.5 + (novoVersoCard.width + 73.5) * (posX), 108 + (novoVersoCard.height + 26) * (posY)
		novoVersoCard.id = i

		if posX < 2 then
			posX = posX + 1
		else
			posX = 0
			posY = posY + 1
		end

		versoCards:insert(novoVersoCard)
	end

	tabela = carregarArquivo("playerCards")

	frenteCards = display.newGroup()

	for a, b in pairs(tabela) do
		for i,v in pairs(tabela[a]) do
			if tabela[a][i] ~= false then
				local novoCard = display.newImage("Cards/" .. tabela[a][i] .. ".png", 0, 0)
				novoCard.anchorX, novoCard.anchorY = 0, 0
				for j = 1, versoCards.numChildren do
					if versoCards[j].id == tonumber(tabela[a][i]) then
						novoCard.x, novoCard.y = versoCards[j].x, versoCards[j].y
					end
				end
				frenteCards:insert(novoCard)
			end
    	end
	end

	-- Botão back
	back = display.newImage("Imagens/TelaCards/back.png", 0, 0)
	back.isVisible = false

	-- Inserindo nos grupos
	scrollView:insert(versoCards)
	scrollView:insert(frenteCards)
	scrollView.isVisible = false
	grupo:insert(scrollView)
	grupo:insert(label)
	grupo:insert(back)

	-- Sons
	sons = {
		bt = audio.loadSound("Músicas/botao.mp3"),	-- Som do botão sendo pressionado
	}
end

---- Show
function cena:show(evento)
	local pai = evento.parent

	if evento.phase == "will" then
		-- Posicionando objetos
		---- Fundo
		fundo.anchorX, fundo.anchorY = 0, 0
		fundo.x, fundo.y = 0, 0

		---- Label
		label.anchorX, label.anchorY = 0.5, 0
		label.x, label.y = largura/2, 0

		---- Back
		back.anchorX, back.anchorY = 0.5, 0.5
		back.x, back.y = back.width/2 + 12, back.height/2 + 6

	elseif evento.phase == "did" then
		-- Mostra banner
		ads.show("banner", {x = 0, y = 5000, appID = appIDBanner, testMode = false})

		-- Tornando os objetos visíveis
		fundo.isVisible = true
		label.isVisible = true
		back.isVisible = true
		scrollView.isVisible = true

		local function voltaMenuInicial(evento)
			canalBotao = audio.play(sons["bt"], {channel = 8})
			-- Esconde os ads
			ads.hide()
			pai:escondeMenuCards()
		end

		-- Adicionando ouvintes de eventos
		back:addEventListener("tap", voltaMenuInicial)
	end
end

---- Destroy
function cena:destroy(evento)
	-- Eliminando tudo da cena da memória
    ---- Valores literais
	largura, altura, tabela = nil, nil, nil
	---- Elementos gráficos
	display.remove(fundo);	fundo = nil;
	display.remove(back);	back = nil;
	display.remove(label);	label = nil;
	versoCards = nil
	frenteCards = nil
	scrollView:removeSelf();	scrollView = nil;
	---- Sons
	canalBotao = nil
    audio.dispose(sons["bt"]);  sons["bt"] = nil;
    sons = nil
end

-- Adicionando ouvintes de eventos a cena
cena:addEventListener("create", cena)
cena:addEventListener("show", cena)
cena:addEventListener("destroy", cena)

return cena