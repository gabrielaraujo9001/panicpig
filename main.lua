---- ******************************** ----
----                                  ----
----       Nome: Panic Pig v0.1       ----
----    Criadores: Daniel, Gabriel    ----
----    Desenvolvido em Corona SDK    ----
----    Tamanho da tela: 960 x 540    ----
----          Proporção 16:9          ----
----                                  ----
----           Arquivo: main          ----
----                                  ----
---- ******************************** ----

-- Escondendo a barra de status do dispositivo
display.setStatusBar(display.HiddenStatusBar)

-- Importando a biblioteca "composer", para gerenciar cenas
local composer = require("composer")
local json = require("json")

-- Declarando as variáveis
local logo, loadSheet, loadSprite, temporizador,resultado

-- Função mostraSplash, acionada após 3 segundos
local function mostraSplash()
	logo:removeSelf();	logo = nil
	loadSprite:removeSelf();	loadSprite = nil

	composer.gotoScene("codigos.splash")
end

-- Criando o fundo que será um pseudo-preloader do jogo, apenas para mostrar a logomarca Red-Nankin
logo = display.newImageRect("Imagens/Loading.jpg", 960, 540)
logo.x, logo.y = display.viewableContentWidth/2,  display.viewableContentHeight/2

-- Criando a sprite que servirá como pseudo-carregamento
---- Sheet
loadSheet = graphics.newImageSheet("Imagens/SpriteLogo.png", {width = 31, height = 8, numFrames = 3})
---- Sprite
loadSprite = display.newSprite(loadSheet, {name = "c", start = 1, count = 3, time = 2000})
loadSprite.anchorX, loadSprite.anchorY = 0, 0
loadSprite.x, loadSprite.y = 535, 455
loadSprite:play()

resultado = composer.loadScene("codigos.splash")

-- Criando o temporizador
temporizador = timer.performWithDelay(6000, mostraSplash, 1)